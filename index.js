'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const atob = require('atob');
const fs = require('fs');
const util = require('util');

const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);

const app = express();
const port = process.env.PORT || 3000;

app.use(bodyParser.json());

app.get('/', (req, res) => res.send({message:'Hello! Im the home endpoint'}));
app.post('/payment/card', validateData, decodeJWT, saveCardInfoOnDB, response);

app.listen(port, () => {
    console.log(`server running and listenning on port ${port}`)
});

function validateData(req, res, next) {
    const { data } = req.body;

    if (!data) {
        return res.status(400).send({
            message:'You need to pass the data',
            code: 400
        });
    }

    
    if (data && data.split('.').length !== 3) {
        return res.status(400).send({
            message: 'The data is not a JWT valid',
            code: 400
        });
    }

    next();
}

function decodeJWT(req, res, next) {
    const { data } = req.body;
    let payload = {};
    const base64Url = data.split('.')[1];
    const base64 = base64Url.replace('-', '+').replace('-', '/');
    payload = JSON.parse(atob(base64));
    req.payload = payload;
    next();
}

async function saveCardInfoOnDB(req, res, next) {
    const dbName = 'db.json';
    const data = await readFile(dbName,{ encoding:'utf8' });
    const db = JSON.parse(data);
    db.push(req.payload);

    await writeFile(dbName, JSON.stringify(db));
    next();
}

function response(req, res) {
    res.status(201).send({
        message: 'Ok',
        code : 201
    });
}