# Oluha Group

This project is an API REST that have only two endpoints:
- [GET] `/` home
- [POST] `/payment/card` save user payment method


## Usage

```sh
1. git clone git@gitlab.com:jorcalle11/oluha-group-api.git
2. cd oluha-group-api
3. npm install
4. npm start
```

Once you have installed and started the application, you can go ahead and try to use the `/payment/card` endpoint.

The endpoint receive the following body: 

```json
{
	"data": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjYXJkX251bWJlciI6IjEyMzQ1IiwiY2FyZF9leHBfbW9udGgiOiIwNSIsImNhcmRfZXhwX3llYXIiOiIyMDI0IiwiY2FyZF9jdmMiOiIxMjMifQ.66fga4J703gCb_eSHnAnN_myHcCt5RmlmkRdYRgDchA"
}
```

The value of the data must be a JWT token with the information of the card that you want to save. It should looks like this:

```json
{
   "card_number": "12345",
   "card_exp_month": "05",
   "card_exp_year": "2024",
   "card_cvc": "123"
}
```

If you want to generate your own JWT, please visit the [JWT site](https://jwt.io/) and enter the data that you want to encrypt in the payload section.

The payment information is stored in a `json` file (`db.json`) located in the root of the project.

That's it

If you have any questions or comment, please feel free to contact me :)
